package servidor;

/**
 * Estas s�o propriedades do cliente.
 * � usada pela classe Server.
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class NetPlayer {

    public int id; // o id do cliente
    public Socket socket; // o socket
    public DataInputStream in; // entrada
    public DataOutputStream out; // sa�da

    public NetPlayer(int id, Socket socket) {
        this.id = id;
        this.socket = socket;
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
        	System.out.println("### ERRO: ao iniciar os sockets ###");
        	e.printStackTrace();
        } 
    }
    
    
    
}
