
package servidor;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

	public static int counter = 0; // Contabiliza as conex�es
	private ArrayList<NetPlayer> netPlayers = new ArrayList<NetPlayer>(); // Array NetPlayer. Possui as propriedades do cliente

	public Server() {

		ServerSocket server;

		System.out.println("ServerSocket inicializado na porta 5000.");

		try {
			server = new ServerSocket(5000); 
			while (true) { // In�cio da verifica��o das conex�es
				if(counter < 2){ // Limite de 2 conex�es, ou 2 jogadores
					Socket socket = server.accept(); // conex�o aceita

					NetPlayer aux = new NetPlayer(counter, socket); // NetPlayer para o cliente conectado
					System.out.println(" *** Player " + counter + " se conectou.");
					counter++; // cliente contabilizado
					netPlayers.add(aux); // cliente listado
					new Thread(new Server.EscutaCliente(aux)).start(); // cria uma thread da classe EscutaCliente e inicia

					if(counter == 2){ // se o numero de jogadores atual for 2
						System.out.println("Servidor cheio. Nenhum cliente a mais poderá se conectar;\n----------------------");
						encaminharParaTodos("start"); // manda ambos iniciarem o jogo
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("### ERRO: ao iniciar o servidor ###");
			ex.printStackTrace();
		}

	}                                    

	/**
	 * M�todo que encaminha para todos os clientes determinada mensagem
	 * @param texto 
	 */
	private void encaminharParaTodos(String texto){
		try {
			for(NetPlayer obj : netPlayers){
				obj.out.writeUTF(texto);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Classe EscutaCliente. Cada cliente tera uma thread executando aqui.
	 * Essa classe ouve a mensagem dos clientes, e envia apenas para o outro cliente(adversario).
	 */
	private class EscutaCliente implements Runnable {

		NetPlayer np;

		public EscutaCliente(NetPlayer np) {
			this.np = np;
		}

		/**
		 * Sobrescrevendo metodo run
		 */
		@Override
		public void run() {
			String s = "";
			try {
				while (true) {
					s = np.in.readUTF(); // le a mensagem
					if(np.id == 0){ // se o cliente desta thread for o 0
						netPlayers.get(1).out.writeUTF(s); // envia a mensagem para o 1
					} else { 
						netPlayers.get(0).out.writeUTF(s); // envia mensagem para o 0
					}
					// DEBUG  System.out.println("id " + np.id + " :" + s);
				}
			} catch (Exception ex) {
			}
		}
	}


	public static void main(String args[]) {
		new Server(); // metodo main chama o construtor da classe Server
	}

}
